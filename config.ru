require './my_middleware'
use MyMiddleware::Hello # this comes in between
run Proc.new{ |env| [404, {"Content-Type" => "text/plain"}, ['ERROR.']] }
