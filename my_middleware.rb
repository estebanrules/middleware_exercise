# Write a Rack middleware that throws a 404 page displaying "Not OK!" whenever the
# client tries to access the app using the wrong path. For example, if the client types the
# URL http://localhost:9292 then the app throws a page displaying "OK!" whereas for
# any other URL like http://localhost:9292/newroute the app throws a 404 page
# displaying "Not OK!"


# my_middleware.rb
module MyMiddleware
  class Hello
    def initialize(app)
      @app = app
    end

    def call(env)
      if env['PATH_INFO'] == '/'
        [200, {"Content-Type" => "text/plain"}, ["OK!"]]
      else
        @app.call(env)
      end
    end
  end
end
